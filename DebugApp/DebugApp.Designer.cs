namespace DebugApp {
	partial class efDebugApp {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.escContainer = new System.Windows.Forms.SplitContainer();
			this.dcgClient = new System.Windows.Forms.GroupBox();
			this.dclOutput = new System.Windows.Forms.Label();
			this.ecbSend = new System.Windows.Forms.Button();
			this.ectMessage = new System.Windows.Forms.TextBox();
			this.ectOutput = new System.Windows.Forms.TextBox();
			this.ecbConnect = new System.Windows.Forms.Button();
			this.eclStatus = new System.Windows.Forms.Label();
			this.dclStatus = new System.Windows.Forms.Label();
			this.dclPort = new System.Windows.Forms.Label();
			this.ecnPort = new System.Windows.Forms.NumericUpDown();
			this.ectHost = new System.Windows.Forms.TextBox();
			this.dclHost = new System.Windows.Forms.Label();
			this.dhgListen = new System.Windows.Forms.GroupBox();
			this.ehlbClients = new System.Windows.Forms.ListBox();
			this.dhlClients = new System.Windows.Forms.Label();
			this.dhlOutput = new System.Windows.Forms.Label();
			this.ehbBroadcast = new System.Windows.Forms.Button();
			this.ehtMessage = new System.Windows.Forms.TextBox();
			this.ehtOutput = new System.Windows.Forms.TextBox();
			this.ehbListen = new System.Windows.Forms.Button();
			this.ehlStatus = new System.Windows.Forms.Label();
			this.dhlStatus = new System.Windows.Forms.Label();
			this.dhlPort = new System.Windows.Forms.Label();
			this.ehnPort = new System.Windows.Forms.NumericUpDown();
			((System.ComponentModel.ISupportInitialize)(this.escContainer)).BeginInit();
			this.escContainer.Panel1.SuspendLayout();
			this.escContainer.Panel2.SuspendLayout();
			this.escContainer.SuspendLayout();
			this.dcgClient.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ecnPort)).BeginInit();
			this.dhgListen.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.ehnPort)).BeginInit();
			this.SuspendLayout();
			// 
			// escContainer
			// 
			this.escContainer.Dock = System.Windows.Forms.DockStyle.Fill;
			this.escContainer.Location = new System.Drawing.Point(0, 0);
			this.escContainer.Name = "escContainer";
			// 
			// escContainer.Panel1
			// 
			this.escContainer.Panel1.Controls.Add(this.dcgClient);
			// 
			// escContainer.Panel2
			// 
			this.escContainer.Panel2.Controls.Add(this.dhgListen);
			this.escContainer.Size = new System.Drawing.Size(907, 562);
			this.escContainer.SplitterDistance = 437;
			this.escContainer.SplitterWidth = 8;
			this.escContainer.TabIndex = 13;
			// 
			// dcgClient
			// 
			this.dcgClient.Controls.Add(this.dclOutput);
			this.dcgClient.Controls.Add(this.ecbSend);
			this.dcgClient.Controls.Add(this.ectMessage);
			this.dcgClient.Controls.Add(this.ectOutput);
			this.dcgClient.Controls.Add(this.ecbConnect);
			this.dcgClient.Controls.Add(this.eclStatus);
			this.dcgClient.Controls.Add(this.dclStatus);
			this.dcgClient.Controls.Add(this.dclPort);
			this.dcgClient.Controls.Add(this.ecnPort);
			this.dcgClient.Controls.Add(this.ectHost);
			this.dcgClient.Controls.Add(this.dclHost);
			this.dcgClient.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dcgClient.Location = new System.Drawing.Point(0, 0);
			this.dcgClient.Name = "dcgClient";
			this.dcgClient.Size = new System.Drawing.Size(437, 562);
			this.dcgClient.TabIndex = 3;
			this.dcgClient.TabStop = false;
			this.dcgClient.Text = "Client";
			// 
			// dclOutput
			// 
			this.dclOutput.AutoSize = true;
			this.dclOutput.Location = new System.Drawing.Point(3, 129);
			this.dclOutput.Name = "dclOutput";
			this.dclOutput.Size = new System.Drawing.Size(55, 13);
			this.dclOutput.TabIndex = 11;
			this.dclOutput.Text = "Messages";
			// 
			// ecbSend
			// 
			this.ecbSend.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.ecbSend.Enabled = false;
			this.ecbSend.Location = new System.Drawing.Point(347, 104);
			this.ecbSend.Name = "ecbSend";
			this.ecbSend.Size = new System.Drawing.Size(84, 23);
			this.ecbSend.TabIndex = 10;
			this.ecbSend.Text = "Send";
			this.ecbSend.UseVisualStyleBackColor = true;
			this.ecbSend.Click += new System.EventHandler(this.ecbSend_Click);
			// 
			// ectMessage
			// 
			this.ectMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ectMessage.Enabled = false;
			this.ectMessage.Location = new System.Drawing.Point(6, 106);
			this.ectMessage.Name = "ectMessage";
			this.ectMessage.Size = new System.Drawing.Size(335, 20);
			this.ectMessage.TabIndex = 9;
			this.ectMessage.WordWrap = false;
			// 
			// ectOutput
			// 
			this.ectOutput.AcceptsReturn = true;
			this.ectOutput.AcceptsTab = true;
			this.ectOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ectOutput.Location = new System.Drawing.Point(3, 145);
			this.ectOutput.Multiline = true;
			this.ectOutput.Name = "ectOutput";
			this.ectOutput.ReadOnly = true;
			this.ectOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.ectOutput.Size = new System.Drawing.Size(431, 414);
			this.ectOutput.TabIndex = 8;
			// 
			// ecbConnect
			// 
			this.ecbConnect.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ecbConnect.Location = new System.Drawing.Point(6, 64);
			this.ecbConnect.Name = "ecbConnect";
			this.ecbConnect.Size = new System.Drawing.Size(425, 23);
			this.ecbConnect.TabIndex = 7;
			this.ecbConnect.Text = "Connect";
			this.ecbConnect.UseVisualStyleBackColor = true;
			this.ecbConnect.Click += new System.EventHandler(this.ecbConnect_Click);
			// 
			// eclStatus
			// 
			this.eclStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.eclStatus.Location = new System.Drawing.Point(347, 90);
			this.eclStatus.Name = "eclStatus";
			this.eclStatus.Size = new System.Drawing.Size(84, 13);
			this.eclStatus.TabIndex = 6;
			this.eclStatus.Text = "disconnected";
			// 
			// dclStatus
			// 
			this.dclStatus.AutoSize = true;
			this.dclStatus.Location = new System.Drawing.Point(3, 90);
			this.dclStatus.Name = "dclStatus";
			this.dclStatus.Size = new System.Drawing.Size(40, 13);
			this.dclStatus.TabIndex = 5;
			this.dclStatus.Text = "Status:";
			// 
			// dclPort
			// 
			this.dclPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.dclPort.AutoSize = true;
			this.dclPort.Location = new System.Drawing.Point(344, 23);
			this.dclPort.Name = "dclPort";
			this.dclPort.Size = new System.Drawing.Size(26, 13);
			this.dclPort.TabIndex = 4;
			this.dclPort.Text = "Port";
			// 
			// ecnPort
			// 
			this.ecnPort.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.ecnPort.Location = new System.Drawing.Point(347, 39);
			this.ecnPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
			this.ecnPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.ecnPort.Name = "ecnPort";
			this.ecnPort.Size = new System.Drawing.Size(84, 20);
			this.ecnPort.TabIndex = 3;
			this.ecnPort.Value = new decimal(new int[] {
            25565,
            0,
            0,
            0});
			// 
			// ectHost
			// 
			this.ectHost.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ectHost.CharacterCasing = System.Windows.Forms.CharacterCasing.Lower;
			this.ectHost.Location = new System.Drawing.Point(6, 38);
			this.ectHost.Name = "ectHost";
			this.ectHost.Size = new System.Drawing.Size(335, 20);
			this.ectHost.TabIndex = 2;
			this.ectHost.Text = "localhost";
			this.ectHost.TextAlign = System.Windows.Forms.HorizontalAlignment.Right;
			this.ectHost.WordWrap = false;
			// 
			// dclHost
			// 
			this.dclHost.AutoSize = true;
			this.dclHost.Location = new System.Drawing.Point(3, 22);
			this.dclHost.Name = "dclHost";
			this.dclHost.Size = new System.Drawing.Size(29, 13);
			this.dclHost.TabIndex = 1;
			this.dclHost.Text = "Host";
			// 
			// dhgListen
			// 
			this.dhgListen.Controls.Add(this.ehlbClients);
			this.dhgListen.Controls.Add(this.dhlClients);
			this.dhgListen.Controls.Add(this.dhlOutput);
			this.dhgListen.Controls.Add(this.ehbBroadcast);
			this.dhgListen.Controls.Add(this.ehtMessage);
			this.dhgListen.Controls.Add(this.ehtOutput);
			this.dhgListen.Controls.Add(this.ehbListen);
			this.dhgListen.Controls.Add(this.ehlStatus);
			this.dhgListen.Controls.Add(this.dhlStatus);
			this.dhgListen.Controls.Add(this.dhlPort);
			this.dhgListen.Controls.Add(this.ehnPort);
			this.dhgListen.Dock = System.Windows.Forms.DockStyle.Fill;
			this.dhgListen.Location = new System.Drawing.Point(0, 0);
			this.dhgListen.Name = "dhgListen";
			this.dhgListen.Size = new System.Drawing.Size(462, 562);
			this.dhgListen.TabIndex = 13;
			this.dhgListen.TabStop = false;
			this.dhgListen.Text = "Host";
			// 
			// ehlbClients
			// 
			this.ehlbClients.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ehlbClients.Enabled = false;
			this.ehlbClients.FormattingEnabled = true;
			this.ehlbClients.Location = new System.Drawing.Point(6, 73);
			this.ehlbClients.Name = "ehlbClients";
			this.ehlbClients.SelectionMode = System.Windows.Forms.SelectionMode.MultiExtended;
			this.ehlbClients.Size = new System.Drawing.Size(450, 82);
			this.ehlbClients.TabIndex = 13;
			this.ehlbClients.SelectedIndexChanged += new System.EventHandler(this.ehlbClients_SelectedIndexChanged);
			// 
			// dhlClients
			// 
			this.dhlClients.AutoSize = true;
			this.dhlClients.Location = new System.Drawing.Point(3, 57);
			this.dhlClients.Name = "dhlClients";
			this.dhlClients.Size = new System.Drawing.Size(93, 13);
			this.dhlClients.TabIndex = 12;
			this.dhlClients.Text = "Connected Clients";
			// 
			// dhlOutput
			// 
			this.dhlOutput.AutoSize = true;
			this.dhlOutput.Location = new System.Drawing.Point(3, 184);
			this.dhlOutput.Name = "dhlOutput";
			this.dhlOutput.Size = new System.Drawing.Size(55, 13);
			this.dhlOutput.TabIndex = 11;
			this.dhlOutput.Text = "Messages";
			// 
			// ehbBroadcast
			// 
			this.ehbBroadcast.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.ehbBroadcast.Enabled = false;
			this.ehbBroadcast.Location = new System.Drawing.Point(372, 159);
			this.ehbBroadcast.Name = "ehbBroadcast";
			this.ehbBroadcast.Size = new System.Drawing.Size(84, 23);
			this.ehbBroadcast.TabIndex = 10;
			this.ehbBroadcast.Text = "Broadcast";
			this.ehbBroadcast.UseVisualStyleBackColor = true;
			this.ehbBroadcast.Click += new System.EventHandler(this.ehbBroadcast_Click);
			// 
			// ehtMessage
			// 
			this.ehtMessage.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ehtMessage.Enabled = false;
			this.ehtMessage.Location = new System.Drawing.Point(6, 161);
			this.ehtMessage.Name = "ehtMessage";
			this.ehtMessage.Size = new System.Drawing.Size(360, 20);
			this.ehtMessage.TabIndex = 9;
			this.ehtMessage.WordWrap = false;
			// 
			// ehtOutput
			// 
			this.ehtOutput.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ehtOutput.Location = new System.Drawing.Point(3, 200);
			this.ehtOutput.Multiline = true;
			this.ehtOutput.Name = "ehtOutput";
			this.ehtOutput.ReadOnly = true;
			this.ehtOutput.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
			this.ehtOutput.Size = new System.Drawing.Size(456, 359);
			this.ehtOutput.TabIndex = 8;
			// 
			// ehbListen
			// 
			this.ehbListen.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.ehbListen.Location = new System.Drawing.Point(372, 18);
			this.ehbListen.Name = "ehbListen";
			this.ehbListen.Size = new System.Drawing.Size(84, 23);
			this.ehbListen.TabIndex = 7;
			this.ehbListen.Text = "Listen";
			this.ehbListen.UseVisualStyleBackColor = true;
			this.ehbListen.Click += new System.EventHandler(this.ehbListen_Click);
			// 
			// ehlStatus
			// 
			this.ehlStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
			this.ehlStatus.Location = new System.Drawing.Point(372, 44);
			this.ehlStatus.Name = "ehlStatus";
			this.ehlStatus.Size = new System.Drawing.Size(84, 13);
			this.ehlStatus.TabIndex = 6;
			this.ehlStatus.Text = "not listening";
			// 
			// dhlStatus
			// 
			this.dhlStatus.AutoSize = true;
			this.dhlStatus.Location = new System.Drawing.Point(3, 44);
			this.dhlStatus.Name = "dhlStatus";
			this.dhlStatus.Size = new System.Drawing.Size(40, 13);
			this.dhlStatus.TabIndex = 5;
			this.dhlStatus.Text = "Status:";
			// 
			// dhlPort
			// 
			this.dhlPort.AutoSize = true;
			this.dhlPort.Location = new System.Drawing.Point(3, 22);
			this.dhlPort.Name = "dhlPort";
			this.dhlPort.Size = new System.Drawing.Size(26, 13);
			this.dhlPort.TabIndex = 4;
			this.dhlPort.Text = "Port";
			// 
			// ehnPort
			// 
			this.ehnPort.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.ehnPort.Location = new System.Drawing.Point(35, 20);
			this.ehnPort.Maximum = new decimal(new int[] {
            65535,
            0,
            0,
            0});
			this.ehnPort.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
			this.ehnPort.Name = "ehnPort";
			this.ehnPort.Size = new System.Drawing.Size(331, 20);
			this.ehnPort.TabIndex = 3;
			this.ehnPort.Value = new decimal(new int[] {
            25565,
            0,
            0,
            0});
			// 
			// efDebugApp
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(907, 562);
			this.Controls.Add(this.escContainer);
			this.Margin = new System.Windows.Forms.Padding(2);
			this.MaximizeBox = false;
			this.Name = "efDebugApp";
			this.Text = "Debug App";
			this.escContainer.Panel1.ResumeLayout(false);
			this.escContainer.Panel2.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.escContainer)).EndInit();
			this.escContainer.ResumeLayout(false);
			this.dcgClient.ResumeLayout(false);
			this.dcgClient.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ecnPort)).EndInit();
			this.dhgListen.ResumeLayout(false);
			this.dhgListen.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.ehnPort)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.SplitContainer escContainer;
		private System.Windows.Forms.GroupBox dcgClient;
		private System.Windows.Forms.Label dclOutput;
		private System.Windows.Forms.Button ecbSend;
		private System.Windows.Forms.TextBox ectMessage;
		private System.Windows.Forms.TextBox ectOutput;
		private System.Windows.Forms.Button ecbConnect;
		private System.Windows.Forms.Label eclStatus;
		private System.Windows.Forms.Label dclStatus;
		private System.Windows.Forms.Label dclPort;
		private System.Windows.Forms.NumericUpDown ecnPort;
		private System.Windows.Forms.TextBox ectHost;
		private System.Windows.Forms.Label dclHost;
		private System.Windows.Forms.GroupBox dhgListen;
		private System.Windows.Forms.ListBox ehlbClients;
		private System.Windows.Forms.Label dhlClients;
		private System.Windows.Forms.Label dhlOutput;
		private System.Windows.Forms.Button ehbBroadcast;
		private System.Windows.Forms.TextBox ehtMessage;
		private System.Windows.Forms.TextBox ehtOutput;
		private System.Windows.Forms.Button ehbListen;
		private System.Windows.Forms.Label ehlStatus;
		private System.Windows.Forms.Label dhlStatus;
		private System.Windows.Forms.Label dhlPort;
		private System.Windows.Forms.NumericUpDown ehnPort;
	}
}

