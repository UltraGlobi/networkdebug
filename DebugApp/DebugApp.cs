using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

// This is the code for your desktop app.
// Press Ctrl+F5 (or go to Debug > Start Without Debugging) to run your app.

namespace DebugApp {
	public partial class efDebugApp : Form {

		#region Props / Fields
		#region Constants
		public const int TASK_DELAY = 33;
		#endregion
		#region Properties
		private static string NL { get => Environment.NewLine; }
		public TcpClient LocalClient { get; set; }
		public TcpListener Listener { get; set; }
		public bool IsListening { get; set; }
		private object ClientListLock { get; set; }
		#endregion
		#region Fields
		private IProgress<bool> clientStatusProgress;
		private IProgress<string> clientMessageProgress;
		private IProgress<bool> hostStatusProgress;
		private IProgress<string> hostMessageProgress;
		#endregion
		#endregion
		#region Methods
		#region Constructor
		public efDebugApp() {
			InitializeComponent();

			ClientListLock = new object();
			
			dhgListen.DoubleClick += (ignore, ignoreAsWell) => escContainer.Panel1Collapsed = !escContainer.Panel1Collapsed;
			dcgClient.DoubleClick += (ignore, ignoreAsWell) => escContainer.Panel2Collapsed = !escContainer.Panel2Collapsed;
			ehlbClients.DisplayMember = "Hash";
			ehlbClients.ValueMember = "Client";

			clientStatusProgress = new Progress<bool>((active) => eclStatus.Text = active ? "connected" : "disconnected");
			clientMessageProgress = new Progress<string>((msg) => {
				int initialLength = ectOutput.Text.Length;
				// save scroll and selection then restore them after applying message
				int ss = ectOutput.SelectionStart;
				int sl = ectOutput.SelectionLength;
				ectOutput.Text += msg + NL;
				if (ss + sl != initialLength && ss + sl != 0) {
					ectOutput.SelectionStart = ss;
					ectOutput.SelectionLength = sl;
					ectOutput.ScrollToCaret();
				} else {
					ectOutput.SelectionStart = ectOutput.Text.Length;
					ectOutput.ScrollToCaret();
				}
			});
			hostStatusProgress = new Progress<bool>((active) => ehlStatus.Text = active ? "connected" : "disconnected");
			hostMessageProgress = new Progress<string>((msg) => {
				int initialLength = ehtOutput.Text.Length;
				// save scroll and selection then restore them after applying message
				int ss = ehtOutput.SelectionStart;
				int sl = ehtOutput.SelectionLength;
				ehtOutput.Text += msg + NL;
				if (ss + sl != initialLength && ss + sl != 0) {
					ehtOutput.SelectionStart = ss;
					ehtOutput.SelectionLength = sl;
					ehtOutput.ScrollToCaret();
				} else {
					ehtOutput.SelectionStart = ehtOutput.Text.Length;
					ehtOutput.ScrollToCaret();
				}
			});
		}
#endregion
#region Helper Methods
		private void DisableElements(params Control[] items) { foreach (Control item in items) item.Enabled = false; }
		private void EnableElements(params Control[] items) { foreach (Control item in items) item.Enabled = true; }
		private void DisableElementsNonUI(params Control[] items) => Invoke(new MethodInvoker(() => { DisableElements(items); }));
		private void EnableElementsNonUI(params Control[] items) => Invoke(new MethodInvoker(() => { EnableElements(items); }));
		private void RegisterClient(TcpClient client) => Invoke(new MethodInvoker(() => {
			lock(ClientListLock) ehlbClients.Items.Add(new ClientListItem(client));
		}));
		private void UnRegisterClient(TcpClient client) => Invoke(new MethodInvoker(() => {
			ClientListItem target = null;
			lock (ClientListLock) {
				foreach (ClientListItem item in ehlbClients.Items)
					if (item.Client == client) {
						target = item;
						break;
					}
				if (target != null) ehlbClients.Items.Remove(target);
			}
		}));
#endregion
#region Event Methods
#pragma warning disable IDE1006 // Event methods violate rule IDE1006 as their names start lower case, but we allow this here.
#region Client Events
		private void ecbSend_Click(object sender, EventArgs e) {
			byte[] message = ectMessage.Text.Select(c => (byte) c).ToArray();
			int len = message.Length;
			try {
				LocalClient.GetStream().Write(message, 0, len);
				ectMessage.Text = "";
			} catch (Exception ex) {
				hostMessageProgress.Report($"Message couldn't be sent. Error:{NL}{ex.Message}");
			}
		}

		private void ecbConnect_Click(object sender, EventArgs e) {
			if (ecbConnect.Text == "Connect") {
				// when connecting; change button label, activate communication interface, deactivate connection interface
				DisableElements(ecbConnect, ectHost, ecnPort);
				ecbConnect.Text = "Disconnect";
				// todo: reactivate connection/disconnection button in clientlisten task
				Task.Run(async () => await ClientListen()).ContinueWith((ignore) => Invoke(new MethodInvoker(() => {
					// when disconnecting; change button label, deactivate communication interface, activate connection interface
					ecbConnect.Text = "Connect";
					EnableElements(ecbConnect, ectHost, ecnPort);
				})));
			} else LocalClient.Close(); // all remaining actions are done automatically
		}
#endregion
#region Host Events
		private void ehbBroadcast_Click(object sender, EventArgs e) {
			byte[] message = ehtMessage.Text.Select(c => (byte) c).ToArray();
			int len = message.Length;
			lock (ClientListLock) {
				foreach (ClientListItem item in ehlbClients.SelectedItems) {
					try {
						item.Client.GetStream().Write(message, 0, len);
					} catch(Exception ex) {
						hostMessageProgress.Report($"Message couldn't be sent to {item.Hash}. Error:{NL}{ex.Message}");
					}
				}
			}
			ehtMessage.Text = "";
		}
		private void ehbListen_Click(object sender, EventArgs e) {
			if (ehbListen.Text == "Listen") {
				// when starting to listen; change button label, disable listener interface, enable communication interface
				ehbListen.Text = "Stop";
				DisableElements(ehbListen, ehnPort);
				Task.Run(async () => await HostListen()).ContinueWith((ignore) => Invoke(new MethodInvoker(() => {
					// when cancelling listen; change button label, enable listener interface, disable communication interface
					ehbListen.Text = "Listen";
					EnableElements(ehbListen, ehnPort);
				})));
				// todo: start hostlisten task, continue with reset of buttons, reactivate disable button in hostlisten.
			} else {
				IsListening = false;// all remaining actions are done automatically
			}
		}
		private void ehlbClients_SelectedIndexChanged(object sender, EventArgs e) => ehbBroadcast.Enabled = ehlbClients.SelectedIndices.Count > 0;
		#endregion
#pragma warning restore IDE1006 // End of event methods
		#endregion
		#region Tasks
		#region Client Tasks
		public async Task ClientListen() {
			try {
				clientMessageProgress.Report($"connecting...");
				bool isConnected = true;
				LocalClient = new TcpClient(ectHost.Text, (int) ecnPort.Value);
				Socket clientSocket = LocalClient.Client;

				IProgress<bool> connectionCheckProgress = new Progress<bool>((update) => isConnected = update);

#pragma warning disable CS4014 // Task runs async on intention. warning unnecessary
				Task.Run(async () => {
					while (isConnected) {
						connectionCheckProgress.Report(LocalClient?.Client?.IsConnected() ?? false);
						await Task.Delay(1000);
					}
				});
#pragma warning restore CS4014

				clientStatusProgress.Report(true);
				try {
					clientMessageProgress.Report($"Connection established...");
					// reactivate connection (now disconnect) button.
					EnableElementsNonUI(ecbConnect, ectMessage, ecbSend);
					while (LocalClient.Connected) {
						while (LocalClient.Available > 0) clientMessageProgress.Report(clientSocket.ReadAvailableAsString());
						// wait a bit to not strain the processor.
						await Task.Delay(TASK_DELAY);
					}
					clientMessageProgress.Report($"Connection closed.");
				} catch (Exception ex) {
					clientMessageProgress.Report($"Error:\tConnection closed:{NL}{ex.Message}");
				} finally {
					// disable before returning
					DisableElementsNonUI(ecbConnect, ectMessage, ecbSend);
					LocalClient?.Dispose();
					LocalClient = null;
				}
			} catch (Exception ex) {
				clientMessageProgress.Report($"Error:\tConnection failed:{NL}{ex.Message}");
			}
			clientStatusProgress.Report(false);
		}
#endregion
#region Host Tasks
		public async Task ClientServerCommunicationTask(TcpClient client) {
			string clientHash = $"{client.GetHashCode():X}";
			try {
				RegisterClient(client);
				bool isConnected = true;
				IProgress<bool> connectionCheckProgress = new Progress<bool>((update) => isConnected = update);
#pragma warning disable CS4014 // Not awaiting
				Task.Run(async () => {
					while (isConnected) {
						connectionCheckProgress.Report(client?.Client?.IsConnected() ?? false);
						await Task.Delay(1000);
					}
				});
#pragma warning restore CS4014
				Socket clientSocket = client.Client;
				hostMessageProgress.Report($"[{clientHash}] successfully connected:");
				while (IsListening && isConnected) {
					while (client.Available > 0) hostMessageProgress.Report($"[{clientHash}] sent:{NL}{clientSocket.ReadAvailableAsString()}");
					await Task.Delay(TASK_DELAY);
				}
				if (IsListening) hostMessageProgress.Report($"[{clientHash}] disconnected");
			} catch (Exception ex) {
				hostMessageProgress.Report($"[{clientHash}]'s connection failed:{NL}{ex.Message}");
			} finally {
				UnRegisterClient(client);
				client?.Dispose();
			}
		}

		public async Task HostListen() {
			try {
				hostMessageProgress.Report($"listening on port {ehnPort.Value.ToString("####")}...");
				Listener = new TcpListener(IPAddress.Any, (int)ehnPort.Value);
				Listener.Start();
				IsListening = true;
				EnableElementsNonUI(ehbListen, ehtMessage, ehlbClients);
				while (IsListening) {
					while (Listener.Pending()) {
						try {
							hostMessageProgress.Report($"Incoming client, attempting to connect...");
							TcpClient newClient = await Listener.AcceptTcpClientAsync();
							ClientServerCommunicationTask(newClient);
						} catch (Exception ex) {
							hostMessageProgress.Report($"New client connection attempt failed:{NL}{ex.Message}");
						}
					}
					await Task.Delay(TASK_DELAY);
				}
				hostMessageProgress.Report($"Connection Closed Manually");
			} catch(Exception ex) {
				hostMessageProgress.Report($"Error:\tListener closed:{NL}{ex.Message}");
			} finally {
				IsListening = false;
				// disable buttons before returning
				DisableElementsNonUI(ehbListen, ehtMessage, ehlbClients);
				Listener?.Server?.Close();
				Listener?.Stop();
				Listener = null;
			}
		}
		#endregion

		#endregion

		#endregion
	}
	/// <summary>
	/// Doesn't work...
	/// </summary>
	/*
	public static class MissingStreamFunctionality {
		public static byte[] ReadAvailableBytes(this Stream @this, int length = 0) {
			byte[] result;
			using (MemoryStream ms = new MemoryStream()) {

				@this.CopyTo(ms);
				result = ms.ToArray();
			}
			return result;
		}
		public static string ReadAvailableAsString(this Stream @this) => new string(@this.ReadAvailableBytes().Select(b => (char) b).ToArray());
	}
	*/
	public static class MissingSocketFunctionality {
		public static byte[] ReadAvailableBytes(this Socket @this) {
			List<byte> result = new List<byte>();
			while (@this.Available > 0) {
				int currentAvailable = @this.Available;
				byte[] buffer = new byte[currentAvailable];
				@this.Receive(buffer, SocketFlags.None);
				result.AddRange(buffer);
			}
			return result.ToArray();
		}
		public static string ReadAvailableAsString(this Socket @this) => new string(@this.ReadAvailableBytes().Select(b => (char) b).ToArray());
	}
	public static class SocketFixExtension {
		public static bool IsConnected(this Socket @this) {
			try { return !(@this.Poll(100, SelectMode.SelectRead) && @this.Available == 0); }
			catch (SocketException) { return false; }
			catch (Exception) { return false; }
		}
	}
	public class ClientListItem {
		public TcpClient Client { get; }
		public string Hash { get => $"{Client.GetHashCode():X}"; }
		public ClientListItem(TcpClient instance) => Client = instance;

		public static implicit operator TcpClient(ClientListItem @this) => @this.Client;
		public static implicit operator ClientListItem(TcpClient client) => new ClientListItem(client);
	}
}
